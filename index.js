// Master branch
'use strict';
const Hapi      = require('hapi');
const Joi       = require('joi');
// const MySQL     = require('mysql');
const Bcrypt    = require('bcrypt');
const dateFormat = require('dateformat');

const configureRoutes  = require('./src/routes/allRoutes');

// const { connection } = require('./src/db');
const connection  = require('./src/db');
// Create a server with a host and port
const server = new Hapi.Server();
 
// const connection = MySQL.createConnection({
//      host: 'localhost',
//      user: 'root',
//      password: '123',
//      database: 'hapi-js_mysql'
// });
 

server.connection({
    host: 'localhost',
    port: 8000
});
 
// connection.connect();
// connection.connect((err) => {
//     if(err){
//       console.log('Error connecting to Db');
//       return;
//     }
//     console.log('DB Connection established');
//   });

// Add the route
server.route({
    method: 'GET',
    path:'/',
    handler: function (request, reply) {
    return reply('hello world');
}
});

// call allRoutes
server.route(configureRoutes);

// Get users list
server.route({
    method: 'GET',
    path: '/users',
    handler: function (request, reply) {
       connection.query('SELECT * FROM users',
       function (error, results, fields) {
       if (error) throw error; 
       reply(results);
    });
  }
});

// Get user Details By ID
server.route({
    method: 'GET',
    path: '/user/{uid}',
    handler: (request, reply) => {
        const uid = request.params.uid;

        connection.query('SELECT * FROM users WHERE uid = "' + uid + '"', 
        function(error,results, fields){
            if(error) throw error;
            if(results.length !=0){
                reply(results);
            }else {
                reply('Did not match any records');
            }
        });
    },
    config: {
        validate: {
            params: {
                uid: Joi.number().integer().required()
            }
        }
    }
});

// Insert  User Details
server.route({
    method: 'POST',
    path: '/user',
    handler: (request, reply) =>{
        const username  = request.payload.username;
        const password  = request.payload.password;
        const email     = request.payload.email; 
        const dob       = dateFormat(request.payload.dob, "yyyy-mm-dd h:MM:ss ");
        
        //Encryption
        var salt = Bcrypt.genSaltSync();
        var encryptedPassword = Bcrypt.hashSync(password, salt);
    
        //Decrypt
        var orgPassword = Bcrypt.compareSync(password, encryptedPassword);

        connection.query('INSERT INTO USERS (username, password, email, dob) VALUES ("'+username+'","'+encryptedPassword+'","'+email+'","'+dob+'")',
        function(error, results, fields){
            if(error) throw error;            
            reply(results);
        });
    },
    config: {
        validate: {
            payload: {
                username: Joi.string().empty(/\s+/).trim().min(4).max(10).required(), 
                password: Joi.string().min(8).max(15).regex(/^[a-zA-Z0-9]{8,15}$/).required(),
                // password: Joi.string().min(8).max(15).regex(/^(?=.*[A-Z])(?=.*[0-9]){8,10}$/).required(),
                email: Joi.string().email().required(),
                dob: Joi.date().iso().less('1-1-2002').required()
            }
        }
    }
});

// Update  User Details
server.route({
    method: 'PUT',
    path: '/user/{uid}',
    handler: (request, reply) =>{
        const uid       = request.params.uid;
        const username  = request.payload.username;
        const email     = request.payload.email; 
        const dob       = dateFormat(request.payload.dob, "yyyy-mm-dd h:MM:ss ");
        
        //Encryption
        var salt = Bcrypt.genSaltSync();
        var encryptedPassword = Bcrypt.hashSync(password, salt);
    
        //Decrypt
        var orgPassword = Bcrypt.compareSync(password, encryptedPassword);

        connection.query('UPDATE USERS SET username="'+username+'", email="'+email+'",dob="'+dob+'"',
        function(error, results, fields){
            if(error) throw error;            
            reply(results);
        });
    },
    config: {
        validate: {
            payload: {
                username: Joi.string().empty(/\s+/).trim().min(4).max(10).required(), 
                email: Joi.string().email().required(),
                dob: Joi.date().iso().less('1-1-2002').required()
            }
        }
    }
});


// Delete Record
server.route({
    method: 'DELETE',
    path: '/user/{uid}',
    handler: (request, reply) => {
        const uid = request.params.uid;
        connection.query('DELETE from users where uid="'+ uid +'"', function(error,result,fields){
            if(error) throw error;

            if(result.affectedRows){
                reply(` Record Deleted Successfully( ${true})`);
            }else{
                reply(false);
            }
        });
    },
    config: {
        validate: {
            params: {
                uid: Joi.number().integer().required()
            }
        }
    }
});

server.start((err) => {
   if (err) {
     throw err;
   }
  console.log('Server running at:', server.info.uri);
});